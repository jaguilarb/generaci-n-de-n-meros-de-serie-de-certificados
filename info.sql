CREATE PROCEDURE "informix".sp_gennumserie (e_bndA INTEGER)
   RETURNING CHAR(20);

   DEFINE s_numserie    CHAR(20);

   IF (e_bndA = 1) THEN
      SELECT '000010'||'999999'||LPAD(MAX(serial) + 1, 8, 0)
         INTO s_numserie
         FROM num_serie WHERE tipo = 'A';

      IF (s_numserie IS NULL) THEN
         INSERT INTO num_serie (serial, tipo) VALUES (1, 'A');
         LET s_numserie = '00001099999900000001';
      ELSE
         UPDATE num_serie SET serial = SUBSTR(s_numserie, 13, 20)
            WHERE tipo = 'A';
      END IF;
   ELIF(e_bndA = 0) THEN
      SELECT '000010'||'000003'||LPAD(MAX(serial) + 1, 8, 0)
         INTO s_numserie
         FROM num_serie WHERE tipo = 'F';

      IF (s_numserie IS NULL) THEN
         INSERT INTO num_serie (serial, tipo) VALUES (1, 'F');
         LET s_numserie = '00001000000300000000';
      ELSE
         UPDATE num_serie SET serial = SUBSTR(s_numserie, 13, 20)
            WHERE tipo = 'F';
      END IF;
   END IF;

   RETURN s_numserie;
END PROCEDURE;
